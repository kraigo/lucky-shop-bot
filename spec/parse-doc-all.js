const fs = require('fs');
const parseDoc = require('./../src/services/parse-doc');
const dirname = './example';

describe("Parse all files", function() {
    var allFiles = [];

    beforeEach(function(done) {
        fs.readdir(dirname, function(err, filenames) {
            expect(err).toBeFalsy();
            let allowFilenameReg = /\.xlsx?/i

            filenames.forEach(function(filename) {
                if (allowFilenameReg.test(filename)) {
                    fs.readFile(dirname + '/' + filename, function(err, data) {
                        parseDoc.parseData(data, function(err, res) {
                            allFiles.push({name: filename, data: res});
                            done();
                        });
                    });
                }
            })
        });
    })

    it('should file exists', function() {
        expect(allFiles.length).toBeTruthy();
    })

    describe('testing file ', function() {
        allFiles.forEach(function(file) {

                let result = file.data;

                it("result should exist", function() {
                    expect(result).toBeDefined();
                    expect(Array.isArray(result)).toBeTruthy();
                })

                it("result should have length", function() {
                    expect(result.length).toBeGreaterThan(0);
                });

                it("result should contains images", function() {
                    result.forEach(function(item) {
                        expect(item.imagesUrl).toBeDefined();
                    })
                });

                it("images is array", function() {
                    result.forEach(function(item) {
                        expect(Array.isArray(item.imagesUrl)).toBeTruthy();
                    })
                });
                
                it("images should contain links", function() {
                    result.forEach(function(item) {
                        item.imagesUrl.forEach(function(image) {
                            expect(image).toMatch(/^http(.+)\.(jpe?g|png|gif)/i);
                        })
                    })
                });

        })
    })
})