const fs = require('fs');
const parseDoc = require('./../src/services/parse-doc');
const dirname = './example';
const filename = 'doc.xls';

describe("Parse excel", function() {    
    var result;

    beforeEach(function(done) {
        fs.readFile(dirname +'/' + filename, function(err, data) {
            parseDoc.parseData(data, function(err, res) {
                result = res;
                done();
            });
        });
    });
    
    it("result should exist", function() {
        expect(result).toBeDefined();
        expect(Array.isArray(result)).toBeTruthy();
    })

    it("result should have length", function() {
        expect(result.length).toBeGreaterThan(0);
    });

    it("result should contains images", function() {
        result.forEach(function(item) {
            expect(item.imagesUrl).toBeDefined();
            expect(item.imagesUrl.length).toBeGreaterThan(0);
        })
    });

    it("shoud contain required filds", function() {
        result.forEach(function(item) {
            expect(item.title).toBeDefined();
            expect(item.url).toBeDefined();
            expect(item.price).toBeDefined();
            expect(item.imagesUrl).toBeDefined();
        })
    })

    it("images is array", function() {
        result.forEach(function(item) {
            expect(Array.isArray(item.imagesUrl)).toBeTruthy();
        })
    });
    
    it("images should contain links", function() {
        result.forEach(function(item) {
            item.imagesUrl.forEach(function(image) {
                expect(image).toMatch(/^http(.+)\.(jpe?g|png|gif)/i);
            })
        })
    });

    it("url should be valid link", function() {
        result.forEach(function(item) {
            expect(item.url).toMatch(/^https?:\/\/.+/i);
        })
    })
        
});

