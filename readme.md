# Lucky Shop Bot

## Requirements

For development, you will only need `Node.js` installed on your environement and `Mongodb` for database


### Environment variables
- TELEGRAPH_TOKEN
- TELEGRAM_TOKEN
- MONGO_CONNECTION

## Install

    $ npm install

## Usage 

    $ npm start

## Configure production

Rename `process.config.example.js` to `process.config.js` and setup env.  

    $ npm install -g pm2
    $ pm2 start process.config.js

