module.exports = {
    apps: [{
        name: "luckyShopBot",
        script: "./src/index.js",
        env: {
            "NODE_ENV": "development",
            "TELEGRAPH_TOKEN": "",
            "TELEGRAM_TOKEN": "",
            "MONGO_CONNECTION": ""
        }
    }]
}