const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;

var schema = new mongoose.Schema({
    chatId: {
        type: 'string',
        unique: true
    },
    currency: 'string'
});

var Model = mongoose.model('Chat', schema);

module.exports = Model;