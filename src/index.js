const path = require('path');
const util = require('util');
const fs = require('fs');
const bot = require('./services/bot');
const parseDoc = require('./services/parse-doc');
const publish = require('./services/publish');
const messanger = require('./services/messanger');
const db = require('./services/db');
const Chat = require('./models/Chat');

fs.readFileAsync = util.promisify(fs.readFile);
fs.unlinkAsync = util.promisify(fs.unlink);

const downloadsPath = path.resolve(__dirname, 'downloads');

if (!fs.existsSync(downloadsPath)) {
    fs.mkdirSync(downloadsPath);
}

bot.on('message', function(message) {
    let chatId = message.chat.id;
    
    if (message.document) {
        processDocument(message);
    }
})

bot.onText(/\/help/, (message) => {
    const chatId = message.chat.id;
    bot.sendMessage(chatId, 'Upload .xls file from https://q-parser.ru/ and I will parse all items');
});


bot.onText(/^\/currency (.+)/, (message, match) => {
    const chatId = message.chat.id;
    const resp = match[1];
    const options = {
        parse_mode: 'Markdown'
    };
    const update = {
        chatId: chatId,
        currency: resp === 'none' ? '' : resp
    }

    bot.sendChatAction(chatId, 'typing');
    Chat.findOneAndUpdate({chatId}, update, {upsert: true})
        .exec()
        .then(res => {
            bot.sendMessage(chatId, `All your next goods will have currency *${resp}*`, options);
        })
});

bot.onText(/^\/currency$/, (message) => {
    const chatId = message.chat.id;
    const options = {
        parse_mode: 'Markdown'
    };

    Chat.findOne({chatId})
        .exec()
        .then(doc => {
            if (doc && doc.currency) {
                bot.sendMessage(chatId, `Your currency is *${doc.currency}*`, options);
            } else {
                bot.sendMessage(chatId, `Your currency not exist`);
            }
        })
});

function processDocument(message) {    

    let chatId = message.chat.id;
    let documentId = message.document.file_id;    
    let _filePath = null;
    let _data = null;
    let chatOptions = {};
    let availableTypes = [
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'text/csv'
    ];

    if (availableTypes.indexOf(message.document.mime_type) === -1) {
        bot.sendMessage(chatId, '⚠️ Incorrect file format ⚠️');
        return;
    }
    Chat.findOne({chatId}).exec()
        .then(function(doc) { 
            if (doc) {
                chatOptions = doc;
            }
            return bot.downloadFile(documentId, downloadsPath);
        })
        .then(function(filePath) {
            _filePath = filePath;
            return fs.readFileAsync(filePath);
        })
        .then(function(fileData) {
            return parseDoc.parseDataAsync(fileData);
        })
        .then(function(data) {
            _data = data;
            if (message.document.mime_type === 'text/csv') {
                parseDoc.decodeEncoding(_data);
            }
            return messanger.sendMessages(chatId, _data, chatOptions)            
        })
        .then(function() {
            return publish.createPage(_data, chatOptions);
        })
        .then(function(response) {
            let message = `Parsed ${_data.length} goods\n`;
            message += `Link${response.length > 1 ? 's' : ''} to telegraph page:\n`;
            message += response.map(res => res.url).join('\n');
            
            bot.sendMessage(chatId, message);

            return fs.unlinkAsync(_filePath);
        })
        .then(function() {
            console.log(`${new Date().toLocaleString()}: send to ${message.chat.username} (${chatId}) - ${_data.length} goods`);
            
        })
        .catch(function(err) {
            var errorMessage = typeof err == 'string' ? err : err.message;
            bot.sendMessage(chatId, '⚠️ There was error during processing ⚠️\n' + errorMessage);
            // bot.sendMessage(64516528, errorMessage + '\n' + err.stack);
            console.error(err);
        })
}