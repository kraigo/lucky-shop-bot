const XLSX = require('xlsx');
const iconv = require('iconv-lite');

module.exports = {
    parseData: parseData,
    parseDataAsync: parseDataAsync,
    decodeEncoding: decodeEncoding
};

function parseData(data, callback) {  
    var workbook = XLSX.read(data, {type:"buffer"});
    var result = [];

    Object.keys(workbook.Sheets).forEach(function(name) {
        const sheet = workbook.Sheets[name];
        const worksheet = XLSX.utils.sheet_to_json(sheet, {header: 0, raw: false});
        let bookImages = null;

        // if (workbook.files) {
        //     bookImages = Object.keys(workbook.files).filter(k => /\.(jpe?g|png|gif)$/.test(k)).sort().map(k => workbook.files[k].asArrayBuffer());
        // }
        
        for (var i = 0, item; i < worksheet.length; i++) {
            item = worksheet[i];
            
            result.push({
                imagesUrl: item['Изображения'] ? item['Изображения'].split(';') : [],
                // imagesUrl: item[0] ? item[0].split(';') : bookImages && bookImages[i] ? [bookImages[i]] : [],
                title: item['Название'] ? item['Название'].replace(/\/(?=\S)/g, '/ ') : '',
                price: item['Цена'],
                category: item['Категория'],
                subcategory: item['Подкатегория'],
                inStock: item['Наличие'],
                url: item['URL'],
                brand: item['Бренд'],
                description: item['Описание'],
                article: item['Артикул'],
                type: item['Вид изделия'],
                size: item['Размер'],
                composition: item['Состав']
            });
        }

    })
    callback(null, result);
}

function parseDataAsync(data) {
    return new Promise(function (resolve, reject) {
        return parseData(data, function (err, result) {
            if (err)
                return reject(err);
            resolve(result);
        });
    });
}


function decodeEncoding(result) {    
    result.forEach(function(item) {
        Object.keys(item).forEach(function(k) {            
            switch(typeof item[k]) {
                case 'string': {
                    item[k] = iconv.encode(iconv.decode(item[k], 'cp1251'), "utf8").toString();
                    break;
                }
            }
        })
    })
}