const Telegraph = require('telegraph-node');
const telegraph = new Telegraph();
const token = process.env.TELEGRAPH_TOKEN;
const maxSize = 64 * 1024;

module.exports = {
    createPage: createPage
}

function createPage(data, chatOptions) {
    let title = 'Lucky shop goodies';
    let options = {};

    let content = data
        .map(function(item) {
            return itemContent(item, chatOptions)
        });

    let contentGroups = [{
        size: 0,
        content: []
    }];

    for (let i = 0; i < content.length; i++) {
        let group = contentGroups[contentGroups.length - 1];
        let item = content[i];
        let itemSize = contentSize(item);
        
        if (group.size + itemSize > maxSize) {
            group = {
                size: 0,
                content: []
            }
            contentGroups.push(group);
        }

        group.size += itemSize;
        group.content = group.content.concat(item);
    }
    
    return Promise.all(
        contentGroups.map(function(group) {
            return telegraph.createPage(token, title, group.content, options)
        })
    );
}

function itemContent(item, chatOptions) {
    return [{
        "tag": "p",
        "children": [
            {
                "tag": "a",
                "attrs": {
                    "href": item.url,
                    "target": "_blank"
                },
                "children": [
                    `${item.price} ${chatOptions.currency} - ${item.title}`
                ]
            }
        ]
    },
    {
        "tag": "figure",
        "children": [
            {
                "tag": "img",
                "attrs": {
                    "src": item.imagesUrl.length ? item.imagesUrl[0] : ''
                }
            },
            {
              "tag": "figcaption",
              "children": [
                item.description
              ]
            }
        ]
    },
    {
        "tag": "p",
        "children": [
            {
                "tag": "br"
            }
        ]
    }]
}

function contentSize(content) {
    return JSON.stringify(content).length * 2;
}