const mongoose = require('mongoose');
const fs = require('fs');
const path = require('path');

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGO_CONNECTION);

const db = mongoose.connection;

db.on('error', function (err) {
    console.error(err);
});

function initModels() {
    let modelsPath = path.join(__dirname, '..', '/models');
    fs.readdirSync(modelsPath)
        .forEach(function (file) {
            if (~file.indexOf('.js')) {
                require(modelsPath + '/' + file);
            }
        });
}

initModels();

module.exports = {
    db
}