const bot = require('./bot');
const maxSize = 200;
const minTitleSize = 20;

module.exports = {
    sendMessages
}

function sendMessages(chatId, data, chatOptions) {
    return data.reduce(function(promise, item) {
        return promise.then(function() {
            let caption = getCaption(item, chatOptions);
            let msg = getMessage(item, chatOptions);
            
            if (caption.length <= maxSize) {
                return bot.sendPhoto(chatId, item.imagesUrl[0], {caption});                
            } else {
                return bot.sendMessage(chatId, msg, {parse_mode: 'HTML'});
            }
        }, function(err) {
            bot.sendMessage(chatId, `⚠️ ${err.message} ⚠️`);
            console.error(err, item);
        })
    }, Promise.resolve());
}

function getCaption(item, chatOptions) {
    let msg = `${item.price} ${chatOptions.currency}\n`;
    let urlPart = `\n${item.url}`;
    let titleLength = maxSize - msg.length - urlPart.length;
    msg += item.title.slice(0, titleLength < minTitleSize ? minTitleSize - 3 : titleLength - 3);
    msg += item.title.length > titleLength ? '...' : '';
    msg += urlPart;
    return msg;
}

function getMessage(item, chatOptions) {
    return `<a href="${item.imagesUrl[0]}">&#8203;</a>${item.price} ${chatOptions.currency}\n${item.title}\n${item.url}`
}