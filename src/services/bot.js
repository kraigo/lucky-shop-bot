
const TelegramBot = require('node-telegram-bot-api');
const TOKEN = process.env.TELEGRAM_TOKEN;
const APP_URL = process.env.APP_URL;

let bot;


if (process.env.NODE_ENV == 'production') {
    let options = {
        webHook: {
            port: process.env.PORT
        }
    }
    bot = new TelegramBot(TOKEN, options);
    bot.setWebHook(`${APP_URL}/bot${TOKEN}`);

} else {
    let options = {
        polling: true
    }
    bot = new TelegramBot(TOKEN, options);
}

module.exports = bot;